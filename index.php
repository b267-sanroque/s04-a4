<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04: Access Modifiers and Encapsulation</title>
	</head>
	<body>
		<h1>Access Modifiers</h1>

		<h2>Building Object</h2>
			<p><?php //echo $building->name; ?></p>

		<!-- You will receive a "Warning: Undefined property: Condominium::$name". -->
		<!-- This is because Condominium no longer inherits the "name" property due to its "private" modifier in the Building class. -->

		<!-- If the modifier is changed again to protected an " Uncaught Error: Cannot access protected property Condominium::$name" -->
		<!-- This is because we cannot directly access properties with protected access modifier, but compare with the previous warning displayed in the browser we are now able to inherit building properties. -->

		<h2>Condominium Object</h2>
			<p><?php //echo $condominium->name; ?></p>

		<h1>Encapsulation</h1>
			<p>The name of the condominium is <?= $condominium->getName(); ?></p>

			<?php $condominium->setName("Enzo Tower"); ?>
			<p>The name of the condominium is <?= $condominium->getName(); ?></p>
			<?php $condominium->setName("Enzo Condo"); ?>

			<p><?php var_dump($building)?></p>
			<p><?php var_dump($condominium)?></p>

		<h1>Building</h1>
			<!-- Activity codes here -->

			<p>The name of the condominium is <?= $building->getName(); ?>.</p>
				<?php $condominium->setFloors(12); ?>
			<p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors.</p>
				<?php $condominium->setAddress("random address"); ?>
			<p>The <?= $building->getName(); ?> is located at The <?= $building->getAddress(); ?>.</p>
				<?php $building->setName("Caswynn Complex"); ?>
			<p>The name of the building has been changed to <?= $building->getName(); ?>.</p>


		<h1>Condominium</h1>
			<!-- Activity codes here -->

			<p>The name of the condominium is <?= $condominium->getName(); ?>.</p>
				<?php $condominium->setFloors(12); ?>
			<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors.</p>
				<?php $condominium->setAddress("random address"); ?>
			<p>The <?= $condominium->getName(); ?> is located at The <?= $condominium->getAddress(); ?>.</p>
				<?php $condominium->setName("Enzo Tower"); ?>
			<p>The name of the condominium has been changed to <?= $condominium->getName(); ?>.</p>

	</body>
</html>
